#!/bin/bash

ZIP=`command -v zip`
if [ -z "$ZIP" ]; then
  echo "Error: zip command not available"
  exit
fi

cd py
$ZIP -r ssabna *
mv ssabna.zip ../
cd ..

if [ ! -f "ssabna.zip" ]; then
  echo "Error: no ssabna.zip, something went wrong!"
else
  rm -rf py/*.pyc
  echo "Done!"
fi
