__author__ = "Subhadeep Karan"
__copyright__ = "Copyright (c) 2017 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Subhadeep Karan"
__email__ = "skaran@buffalo.edu"
__status__ = "Development"

from pyspark import AccumulatorParam
from pyspark import PickleSerializer
from pyspark import SparkConf
from pyspark import SparkContext

import argparse
from bitarray import bitarray
from collections import namedtuple
import csv
import itertools
import math
import os
import sys
import time

sys.path.insert(0, 'SparkFiles.getRootDirectory()')

import bitarrayFunc as bt
import MPSList as mps
import MDLEngine as mdl_engine

from sabnatk.RadCounter import MDL64
from sabnatk.RadCounter import MDL128
from sabnatk.RadCounter import MDL256

MPSNode = namedtuple('MPSNode', ['Pa', 'score'])
AccumNode = namedtuple('AccumNode', ['xi', 'MPSNode'])
TaskNode = namedtuple('TaskNode', ['Pa', 'Ch'])



class MPSListAccumulator(AccumulatorParam):
    def zero(self, _): return []
    def addInPlace(self, ls1, ls2): return ls1 + ls2


def check_score_status_update_accum(score_status, set_xi, pa):
    global accumMPSList

    for tup in score_status:
        if tup[1] > 0: accumMPSList += [AccumNode(tup[0], MPSNode(pa, tup[1]))]

    return

def process_task(task, score_engine, l):
    global bdMPSList
    set_xi, score_status = score_engine.process(task.Ch, task.Pa, bdMPSList.value, l, False)
    check_score_status_update_accum(score_status, set_xi, task.Pa)
    if not set_xi.any(): return None, None

    task = TaskNode(Pa = task.Pa, Ch = set_xi)
    xmax = bt.max_set_bit(task.Pa, n)

    return task, xmax

def graph_traversal(task, score_engine, l, is_bfs):
    task, xmax = process_task(task, score_engine, l)
    if xmax is None: return

    for xi in xrange(xmax + 1, n):
        Pa = bt.set_add(task.Pa, xi)
        Ch = bt.set_remove(task.Ch, xi)
        if Ch.any():
            if is_bfs: yield (Pa, Ch)
            else: [x for x in graph_traversal(TaskNode(Pa, Ch), score_engine, l + 1, is_bfs)]

    return

def populate_next_layer(it):
    global bdBFS

    score_engine = init_engine()

    for nd in it:
        if not nd: continue

        is_bfs = bdBFS.value

        task = TaskNode(Pa = nd[0], Ch = nd[1])
        next_layer = None
        if is_bfs: next_layer = graph_traversal(task, score_engine, bdL.value, is_bfs)
        else: next_layer = [x for x in graph_traversal(task, score_engine, bdL.value, is_bfs)]

        for ntask in next_layer: yield ntask

    return

def init_engine():
    if n < 1 or n > 256: sys.exit(-1)

    mdl = None
    if n <= 64: mdl = MDL64()
    elif n <= 128: mdl = MDL128()
    elif n <= 256: mdl = MDL256()

    mdl.init(n, m, D)
    score_engine = mdl_engine.MDLEngine(n, m, mdl, options['pasize'])

    return score_engine

def build_mps(n, m, D, lDFS, sc):
    log4jLogger = sc._jvm.org.apache.log4j
    logger = log4jLogger.LogManager.getLogger("SparkingSABNA")

    global bdL
    global bdMPSList
    global accumMPSList
    global bdBFS

    noPar = options['nnodes'] * options['ncores'] * options['nparts']

    start = time.time()
    mpsList = mps.MPSList(n)

    accumMPSList = sc.accumulator([], MPSListAccumulator())
    A = [(bt.empty_bitarray(n), bt.full_bitarray(n))]

    done = False
    C = [sc.parallelize(A)]
    l = 0
    query_count = len(A)

    while l < n and not done:
        if l >= options['ldfs']: bdBFS = sc.broadcast(False)
        else: bdBFS = sc.broadcast(True)

        bdL = sc.broadcast(l)
        bdMPSList = sc.broadcast(mpsList)
        C.append(C[l].repartition(noPar) \
                .mapPartitions(populate_next_layer, preservesPartitioning = True) \
                .cache())
        C[l].unpersist()
        C[l] = None

        taskCount = C[l + 1].count()
        query_count += taskCount
        logger.info('layer {} processed, time elapsed: {}s, size of next layer: {}'.format(l, time.time() - start, taskCount))
        if taskCount <= 0: done = True

        if accumMPSList.value:
            logger.info('updating MPS with {} elements'.format(len(accumMPSList.value)))
            mpst = time.time()
            for nd in accumMPSList.value: mpsList.fast_insert(nd.xi, nd.MPSNode.Pa, nd.MPSNode.score)
            accumMPSList.value = []
            logger.info('update done in {}s'.format(time.time() - mpst))

        l += 1

    logger.info('total BFS tasks count: {}'.format(query_count))

    score_engine = init_engine()
    score_engine.finalize(mpsList)

    logger.info('rebuilding MPS...')
    mpst = time.time()
    if l >= options['ldfs']: mpsList.verify_rebuild()
    logger.info('rebuilding done in {}s'.format(time.time() - mpst))

    mpsList.write(options['output'])

    #with open(options['output'], 'a') as f: f.write('time elapsed: {}s\n'.format(time.time() - start))

    return


def disable_spark_logs(sc):
    logger = sc._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel( logger.Level.ERROR )
    logger.LogManager.getLogger("akka").setLevel( logger.Level.ERROR )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("input", help = "input csv file")
    parser.add_argument("output", help = "output mps file")
    parser.add_argument("nnodes", help = "number of spark nodes", type = int)
    parser.add_argument("ncores", help = "number of cores per spark node", type = int)
    parser.add_argument("nparts", help = "number of partitions per core", type = int)
    parser.add_argument("-d", "--dfsl", help = "level to switch to DFS (default 10)", nargs ='?', type = int, const = 10, default = 10)
    parser.add_argument("-l", "--pa", help = "parent set size limit (default none)", nargs = '?', type = int, const = -1, default = -1)

    args = parser.parse_args()

    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(-1)

    args = parser.parse_args()

    global options
    options = {}

    options['input'] = args.input
    options['output'] = args.output
    options['nnodes'] = args.nnodes
    options['ncores'] = args.ncores
    options['nparts'] = args.nparts
    options['ldfs'] = args.dfsl
    options['pasize'] = args.pa

    global D
    global n

    n = m = 0
    D = []

    with open(options['input'], "rt") as cf:
        rd = csv.reader(cf, delimiter = ' ')
        for row in rd:
            n += 1
            m = len(row)
            D = D + map(int, row)

    if options['pasize'] < 0: options['pasize'] = n

    conf = SparkConf().setAppName("ssabna-mpsbuild")
    sc = SparkContext(conf = conf, serializer=PickleSerializer())
    disable_spark_logs(sc)

    log4jLogger = sc._jvm.org.apache.log4j
    logger = log4jLogger.LogManager.getLogger("SparkingSABNA")

    logger.info('variables: {}, instances: {}'.format(n, m))
    logger.info('ldfs: {}, pasize: {}'.format(options['ldfs'], options['pasize']))

    build_mps(n, m, D, options, sc)

    sc.stop()
    logger.info('finished!')
