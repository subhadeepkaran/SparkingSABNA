__author__ = "Subhadeep Karan"
__copyright__ = "Copyright (c) 2017 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Subhadeep Karan"
__email__ = "skaran@buffalo.edu"
__status__ = "Development"

from pyspark import PickleSerializer
from pyspark import SparkConf
from pyspark import SparkContext

import argparse
from bitarray import bitarray
from collections import namedtuple
import csv
import itertools
import math
import sys
import time

sys.path.insert(0, 'SparkFiles.getRootDirectory()')

import bitarrayFunc as bt
import MPSList as mps
import MDLEngine as mdl_engine


def ope(node_id, score, path):
    done = False
    while not done:
        done = True
        for xi in xrange(n):
            if node_id[xi]: continue
            if optimal_scores[xi][1] & node_id == optimal_scores[xi][1]:
                node_id = bt.set_add(node_id, xi)
                score += optimal_scores[xi][0]
                path += str(xi) + ' '
                done = False

    return node_id, score, path

def populate_next_layer(it):
    ht = {}
    for task in it:
        for xi in xrange(n):
            node_id = bt.string_to_bitarray(task[0])[:n]

            ch_id = task[0]
            path = task[1][1]
            score = task[1][0]
            temp = None

            if node_id.count() == bdL.value:
                if node_id[xi]: continue
                res = mps_list.find(xi, node_id)
                score += res[0]
                path += str(xi) + ' '
                ch_id = bt.set_add(node_id, xi)
                ch_id, score, path = ope(ch_id, score, path)
                ch_id = ch_id.tobytes()

            temp = ht.get(ch_id, None)
            if not temp: ht[ch_id] = (score, path)
            elif temp[0] > score: ht[ch_id] = (score, path)

    for node_id, score_path in ht.items(): yield (node_id, score_path)

    return

def reduction(x, y):
    if x[0] < y[0]: return x
    return y

def core(sc):
    log4jLogger = sc._jvm.org.apache.log4j
    logger = log4jLogger.LogManager.getLogger("SparkingSABNA")

    A = [(bt.empty_bitarray(n).tobytes(), (0, ''))]
    C = [sc.parallelize(A)]
    l = 0

    noPar = nnodes * ncores * nparts
    done = False
    query_count = 0

    start = time.time()

    global bdMPSList
    bdMPSList = sc.broadcast(mps_list)

    global bdL
    while not done:
        bdL = sc.broadcast(l)

        C.append(C[l].repartition(noPar) \
                .mapPartitions(populate_next_layer, preservesPartitioning = True) \
                .reduceByKey(reduction).cache())
        C[l].unpersist()
        C[l] = None

        taskCount = C[l + 1].count()
        query_count += taskCount

        logger.info('layer {} processed, time elapsed {}s, size of next layer: {}'.format(l, time.time() - start, taskCount))

        if taskCount <= 1:
            res = C[l + 1].take(1)[0][1]
            logger.info('optimal network score: {}'.format(res[0]))

            with open(oname, 'w') as f:
                f.write('{}'.format(res[1]))

            done = True

        l += 1

    return


def disable_spark_logs(sc):
    logger = sc._jvm.org.apache.log4j
    logger.LogManager.getLogger("org"). setLevel( logger.Level.ERROR )
    logger.LogManager.getLogger("akka").setLevel( logger.Level.ERROR )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("input", help = "input mps file")
    parser.add_argument("output", help = "output ord file")
    parser.add_argument("n", help = "number of variables", type = int)
    parser.add_argument("nnodes", help = "number of spark nodes", type = int)
    parser.add_argument("ncores", help = "number of cores per spark node", type = int)
    parser.add_argument("nparts", help = "number of partition per core", type = int)

    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(-1)

    args = parser.parse_args()

    global n
    global oname
    global mps_list
    global optimal_scores
    global nnodes
    global ncores
    global nparts

    fname = args.input
    oname = args.output
    n = args.n
    nnodes = args.nnodes
    ncores = args.ncores
    nparts = args.nparts

    mps_list = mps.MPSList(n)
    mps_list.read(fname)
    optimal_scores = []
    for xi in xrange(n): optimal_scores.append(mps_list.optimal_score(xi))

    conf = SparkConf().setAppName("ssabna-bfs-ope")
    sc = SparkContext(conf = conf, serializer=PickleSerializer())
    disable_spark_logs(sc)

    log4jLogger = sc._jvm.org.apache.log4j
    logger = log4jLogger.LogManager.getLogger("SparkingSABNA")

    core(sc)

    sc.stop()
    logger.info('finished!')
