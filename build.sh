#!/bin/bash

# SET HEADERS AND LIBRARIES IF YOUR Boost.Python OR Python INSTALL
# ARE NOT DEFAULT

# Headers
#HEADERS="-DBOOST_INCLUDEDIR=path_to_boost_include"

# Libraries
#LIBRARIES="-DBOOST_LIBRARYDIR=path_to_boost_lib"

# EXAMPLE (SETTING BOOST AND PYTHON):
#HEADERS="-DBOOST_INCLUDEDIR=$BOOST_INC_DIR -DPYTHON_INCLUDE_DIR=$PYTHONDIR/include/python2.7"
#LIBRARIES="-DBOOST_LIBRARYDIR=$BOOST_LIB_DIR -DPYTHON_LIBRARY=$PYTHONDIR/lib"



# --- DO NOT EDIT BELOW ---
echo "Install..."

ZIP=`command -v zip`
if [ -z "$ZIP" ]; then
  echo "Error: zip command not available"
  exit
fi

DIR=`pwd`

mkdir -p sabnatk/build/
rm -rf sabnatk/build/*
cd sabnatk/build/

cmake ../ -DCMAKE_INSTALL_PREFIX=$DIR $HEADERS $LIBRARIES && make install

cd $DIR

if [ -f sabnatk.so ]; then
  cd py
  $ZIP -r ssabna *
  mv ssabna.zip ../
  cd ..
else
  echo "Error: SABNAtk failed to build, Python issue?"
  exit
fi

if [ ! -f "ssabna.zip" ]; then
  echo "Error: no ssabna.zip, something went wrong!"
else
  rm -rf sabnatk/build
  rm -rf py/*.pyc
  echo "Done!"
fi
