/***
 *  $Id$
 **
 *  File: sabnatk.cpp
 *  Created: Nov 05, 2017
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2017 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <boost/python.hpp>

namespace bpy = boost::python;

extern void export_BVCounter();
extern void export_RadCounter();


BOOST_PYTHON_MODULE(sabnatk)
{

    bpy::object package = bpy::scope();
    package.attr("__path__") = "sabnatk";

    export_BVCounter();
    export_RadCounter();

}
