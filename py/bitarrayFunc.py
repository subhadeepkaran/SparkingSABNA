__author__ = "Subhadeep Karan"
__copyright__ = "Copyright (c) 2017 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Subhadeep Karan"
__email__ = "skaran@buffalo.edu"
__status__ = "Development"

from bitarray import bitarray


def set_remove(U, i):
    res = U.copy()
    res[i] = 0

    return res

def set_add(U, i):
    res = U.copy()
    res[i] = 1

    return res

def max_set_bit(U, n):
    xmax = n - 1
    while xmax > -1 and not U[xmax]: xmax -= 1

    return xmax

def string_to_bitarray(U, n = -1):
    res = bitarray(endian = "little")
    res.frombytes(U)
    if n != -1 and n > 0: return res[:n]
    return res

def list_to_bitarray(l, n):
    res = empty_bitarray(n)
    for x in l: res[int(x)] = 1

    return res

def as_list(U):
    n = len(U)
    res = []
    for i in xrange(n):
        if U[i]: res.append(i)

    return res

def empty_bitarray(n): return bitarray('0', endian = "little") * n

def full_bitarray(n): return bitarray('1', endian = "little") * n
