__author__ = "Subhadeep Karan"
__copyright__ = "Copyright (c) 2018 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Subhadeep Karan"
__email__ = "skaran@buffalo.edu"
__status__ = "Development"

def empty_bitset(): return 0

def full_bitset(n):
    temp = set_add(empty_bitset(), n+1)
    temp -= 1
    return temp >> 1
    
def in_set(U, i):
    temp = set_add(empty_bitset(), i)
    return (temp & U) == temp

def set_add(U, i): 
    return U | (1 << i)

def set_remove(U, i):
    if not in_set(U, i): return U
    temp = set_add(empty_bitset(), i)
    return U - temp

def max_set_bit(U):
    res = -1
    while U > 0:
        U >>= 1
        res += 1

    return res

def as_list(U):
    res = []
    i = 0
    while U > 0:
        if in_set(U, i): 
            res.append(i)
            U = set_remove(U, i)
        i += 1
    return res

def count(U): return len(as_list(U))

def list_to_bitset(U):
    X = empty_bitset()
    for i in U: X = set_add(X, i)
    return X

def as_string(U): 
    return ' '.join([str(x) for x in as_list(U)])
        
