__author__ = "Subhadeep Karan"
__copyright__ = "Copyright (c) 2017 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Subhadeep Karan"
__email__ = "skaran@buffalo.edu"
__status__ = "Development"

import bitarrayFunc as bt

import bisect
from collections import namedtuple
from functools import total_ordering


class MPSList:
    n = None
    mps_list = None

    @total_ordering
    class mps_ordering(object):
        __slots__ = ()
        def __lt__(self, other):
            return self.s > other.s

    class mps_node(mps_ordering, namedtuple('mps_node', 's pa')):
        pass

    def __init__(self, n):
        self.n = n
        self.mps_list = [[] for _ in xrange(self.n)]

        return

    def verify_rebuild(self):
        for xi in xrange(self.n):
            idx = set()
            for i in xrange(len(self.mps_list[xi])):
                node = self.mps_list[xi][i]
                for j in xrange(i+1, len(self.mps_list[xi])):
                    node2 = self.mps_list[xi][j]
                    if node2.pa & node.pa == node2.pa: idx.add(i)

            idx = list(idx)
            idx.sort(reverse = True)
            for i in idx: del self.mps_list[xi][i]

        return

    def optimal_score(self, xi): return (self.mps_list[xi][-1].s, self.mps_list[xi][-1].pa)

    def find(self, xi, pa):
        for idx in xrange(len(self.mps_list[xi]) - 1, -1, -1):
            if pa & self.mps_list[xi][idx].pa == self.mps_list[xi][idx].pa: return (self.mps_list[xi][idx].s, self.mps_list[xi][idx].pa, idx)

        return (float('inf'), None, -1)

    def insert(self, xi, pa, score):
        idx = len(self.mps_list[xi]) - 1
        for _ in xrange(len(self.mps_list[xi]) - 1, -1, -1):
            if score < self.mps_list[xi][idx].s: break
            if pa & self.mps_list[xi][idx].pa == self.mps_list[xi][idx].pa:
                if score < self.mps_list[xi][idx].s: break
                else: return True
            idx -= 1
        self.mps_list[xi].insert(idx+1, self.mps_node(s = score, pa = pa))

        return True

    def fast_insert(self, xi, pa, score):
        idx = bisect.bisect_right(self.mps_list[xi], self.mps_node(s = score, pa = pa))
        self.mps_list[xi].insert(idx, self.mps_node(s = score, pa = pa))

    def mps_node_to_string(self, xi, node):
        count = 0
        for xj in xrange(self.n):
            if node.pa[xj]: count += 1

        return "mps " + str(xi) + " " + str(node.s) + " " + str(count) + " " + ' '.join([str(x) for x in bt.as_list(node.pa)])

    def write(self, fname):
        with open(fname, 'w') as f:
            f.write("MPSList_Begins\n")
            for xi in xrange(self.n):
                for node in self.mps_list[xi]: f.write(self.mps_node_to_string(xi, node) + "\n")
                if len(self.mps_list[xi]) == 0: f.write(self.mps_node_to_string(xi, self.mps_node(s = 0, pa = bt.empty_bitarray(self.n))) + "\n")
            f.write("MPSList_Ends\n")

        return

    def map_variables(self, norder):
        res = [[] for _ in xrange(self.n)]

        for i in xrange(self.n):
            for node in self.mps_list[i]:
                old_pa = bt.as_list(node.pa)
                pa = [norder[xi] for xi in old_pa]
                set_pa = bt.list_to_bitarray(pa, self.n)
                res[norder[i]].append(self.mps_node(node.s, set_pa))

        self.mps_list = res

        return

    def read(self, fname):
        temp = {}
        with open(fname, 'r') as f:
            for l in f:
                if "MPSList_Begins" in l: break

            for l in f:
                if "MPSList_Ends" in l: break
                l = l.replace('\n', '').replace('mps ', '').split(' ')
                xi = int(l[0])
                if xi not in temp: temp[xi] = []
                pa_size = int(l[2])
                pa = []
                if pa_size > 0: pa = [int(x) for x in l[3 : 3 + pa_size]]
                set_pa = bt.list_to_bitarray(pa, self.n)
                node = self.mps_node(s = float(l[1]), pa = set_pa)
                temp[xi].append(node)

        for xi in xrange(self.n): self.mps_list[xi] = temp[xi]

        return
