__author__ = "Subhadeep Karan"
__copyright__ = "Copyright (c) 2017 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Subhadeep Karan"
__email__ = "skaran@buffalo.edu"
__status__ = "Development"

import bitarrayFunc as bt


class MDLEngine:
    n = m = None
    score = None
    H = None
    mdl = None
    reordered = None
    max_pa_size = None
    pasize = None
    ordering = None

    def __init__(self, n, m, mdl, pasize):
        self.n = n
        self.m = m
        self.mdl = mdl
        self.pasize = pasize
        self.H = [-1] * self.n
        self.E = bt.empty_bitarray(self.n)
        self.X = bt.full_bitarray(self.n)
        self.max_pa_size = [self.E for _ in xrange(self.n + 1)]
        self.max_pa_size[0] = self.X
        self.reordered = False
        self.init_entropy()
        self.reorder_counting_query_engine()
        self.compute_max_depth()

        return

    def process(self, set_xi, pa, mps_list, l, insert_mpsList = True):
        score_status = []
        res = self.mdl.score(set_xi.tobytes(), pa.tobytes())
        idx_xi = 0
        for xi in xrange(self.n):
            if not set_xi[xi]: continue
            score_status.append((xi, self.extend_insert(xi, pa, res[idx_xi], mps_list, insert_mpsList)))
            if score_status[-1][1] == -1: set_xi = bt.set_remove(set_xi, xi)
            idx_xi += 1
        set_xi = self.max_pa_size[l + 1] & set_xi

        return set_xi, score_status

    # > 0 - mps_node, 0 - non-mps node, -1 - node to be pruned
    def extend_insert(self, xi, pa, score, mps_list, insert_mpsList):
        temp = mps_list.find(xi, pa)
        if score[0] < temp[0]:
            if insert_mpsList: mps_list.insert(xi, pa, score[0])
            return score[0]
        if score[0] - score[1] + self.H[xi] >= temp[0]: return -1
        return 0

    def finalize(self, mps_list):
        if self.reordered: mps_list.map_variables(self.ordering)
        return

    def init_entropy(self):
        for xi in xrange(self.n):
            res = self.mdl.score(bt.set_add(self.E, xi).tobytes(), bt.set_remove(self.X, xi).tobytes())
            self.H[xi] = res[0][1]

        return

    def reorder_counting_query_engine(self):
        temp = [(self.mdl.r(xi), self.H[xi] * -1, xi) for xi in xrange(self.n)]
        temp.sort(reverse = True)
        self.ordering = [x[2] for x in temp]
        self.H = [self.H[xi] for xi in self.ordering]
        self.reordered = self.mdl.reorder(self.ordering)
        self.reordered = True

        return

    def compute_max_depth(self):
        res = self.mdl.score(self.X.tobytes(), self.E.tobytes())
        temp = [(xi, self.mdl.r(xi)) for xi in xrange(self.n)]
        temp.sort(key = lambda x : x[1])

        for xi in xrange(self.n):
            thres, val = res[xi]
            val = thres - val
            thres -= self.H[xi]

            l = 0
            for i in xrange(self.n):
                xj, r_xj = temp[i]
                if xi == xj: continue
                val *= r_xj
                if val >= thres: break
                l += 1
                if l > self.pasize : continue
                self.max_pa_size[l] = bt.set_add(self.max_pa_size[l], xi)

        return
